webpackJsonp([1],{

/***/ 185:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


var __extends = undefined && undefined.__extends || function () {
    var extendStatics = Object.setPrototypeOf || { __proto__: [] } instanceof Array && function (d, b) {
        d.__proto__ = b;
    } || function (d, b) {
        for (var p in b) {
            if (b.hasOwnProperty(p)) d[p] = b[p];
        }
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() {
            this.constructor = d;
        }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
}();
exports.__esModule = true;
var React = __webpack_require__(50);
__webpack_require__(186);
var Demo = /** @class */function (_super) {
    __extends(Demo, _super);
    function Demo() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    Demo.prototype.render = function () {
        return React.createElement("div", { className: "Demo" }, React.createElement("p", null, "ASP.NET + React, from PoznajProgramowanie.pl Hi Core!"));
    };
    return Demo;
}(React.Component);
exports["default"] = Demo;

/***/ }),

/***/ 186:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 84:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


exports.__esModule = true;
var ReactDOM = __webpack_require__(51);
var React = __webpack_require__(50);
var Demo_1 = __webpack_require__(185);
ReactDOM.render(React.createElement(Demo_1["default"], null), document.getElementById('react-homepage-root'));
if (false) {
    module.hot.accept();
}

/***/ })

},[84]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi9GZWF0dXJlcy9Ib21lL1JlYWN0L0RlbW8vQ29tcG9uZW50cy9EZW1vLnRzeCIsIi4uXFwuLlxcLi9GZWF0dXJlcy9Ib21lL1JlYWN0L0RlbW8vQ29tcG9uZW50cy9EZW1vLnNjc3MiLCIuLlxcLi5cXC4vRmVhdHVyZXMvSG9tZS9SZWFjdC9EZW1vL2luZGV4LnRzeCJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsZ0NBQStCO0FBQy9CLG9CQUFxQjtBQUVyQjtBQUFtQixvQkFBeUI7QUFBNUM7bUVBWUE7QUFBQztBQVZVLG1CQUFNLFNBQWI7QUFDVyxlQUNILDZCQUFjLFdBQU8sVUFDakIsK0JBTVo7QUFBQztBQUNMLFdBQUM7QUFBQSxFQVp1QixNQVl2QjtBQUNELHFCQUFvQixLOzs7Ozs7O0FDaEJwQix5Qzs7Ozs7Ozs7Ozs7QUNBQSxtQ0FBc0M7QUFDdEMsZ0NBQStCO0FBRS9CLGlDQUFxQztBQUU3QixTQUFPLE9BQUMsb0JBQUMsT0FBSSxZQUFHLE9BQVUsU0FBZSxlQUF5QjtBQUd2RSxJQUFPLEtBQUssRUFBRTtBQUNQLFdBQUksSUFDZDtBQUFDLEMiLCJmaWxlIjoiaG9tZS5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCAqIGFzIFJlYWN0IGZyb20gJ3JlYWN0JztcclxuaW1wb3J0ICcuL0RlbW8uc2Nzcyc7XHJcblxyXG5jbGFzcyBEZW1vIGV4dGVuZHMgUmVhY3QuQ29tcG9uZW50PGFueSwgYW55PntcclxuXHJcbiAgICBwdWJsaWMgcmVuZGVyKCkge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiRGVtb1wiPlxyXG4gICAgICAgICAgICAgICAgPHA+XHJcbiAgICAgICAgICAgICAgICAgICAgQVNQLk5FVCArIFJlYWN0LCBmcm9tIFBvem5halByb2dyYW1vd2FuaWUucGxcclxuICAgICAgICAgICAgICAgICAgICBIaSBDb3JlIVxyXG4gICAgICAgICAgICAgICAgPC9wPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICApXHJcbiAgICB9XHJcbn1cclxuZXhwb3J0IGRlZmF1bHQgRGVtbztcclxuXG5cblxuLy8gV0VCUEFDSyBGT09URVIgLy9cbi8vIC4vRmVhdHVyZXMvSG9tZS9SZWFjdC9EZW1vL0NvbXBvbmVudHMvRGVtby50c3giLCIvLyByZW1vdmVkIGJ5IGV4dHJhY3QtdGV4dC13ZWJwYWNrLXBsdWdpblxuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIC4vRmVhdHVyZXMvSG9tZS9SZWFjdC9EZW1vL0NvbXBvbmVudHMvRGVtby5zY3NzXG4vLyBtb2R1bGUgaWQgPSAxODZcbi8vIG1vZHVsZSBjaHVua3MgPSAxIiwiaW1wb3J0ICogYXMgUmVhY3RET00gZnJvbSAncmVhY3QtZG9tJztcclxuaW1wb3J0ICogYXMgUmVhY3QgZnJvbSAncmVhY3QnO1xyXG5cclxuaW1wb3J0IERlbW8gZnJvbSAnLi9Db21wb25lbnRzL0RlbW8nO1xyXG5cclxuUmVhY3RET00ucmVuZGVyKDxEZW1vIC8+LCBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgncmVhY3QtaG9tZXBhZ2Utcm9vdCcpKTtcclxuXHJcbmRlY2xhcmUgdmFyIG1vZHVsZTogYW55O1xyXG5pZiAobW9kdWxlLmhvdCkge1xyXG4gICAgbW9kdWxlLmhvdC5hY2NlcHQoKTtcclxufVxuXG5cbi8vIFdFQlBBQ0sgRk9PVEVSIC8vXG4vLyAuL0ZlYXR1cmVzL0hvbWUvUmVhY3QvRGVtby9pbmRleC50c3giXSwic291cmNlUm9vdCI6IiJ9