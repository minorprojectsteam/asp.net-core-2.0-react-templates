﻿using Microsoft.AspNetCore.Mvc.ApplicationModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Templates.React.Configuration.Conventions
{
    public class FeaturesConvention : IControllerModelConvention
    {
        private const string features = "features";

        public void Apply(ControllerModel controller)
        {
            controller.Properties.Add(features, GetFeatureName(controller.ControllerType.FullName));
        }

        private string GetFeatureName(string fullName)
        {
            var tokens = fullName.Split('.');

            if (!tokens.Any(t => t.ToLower() == features))
            {
                return string.Empty;
            }

            return tokens
                .SkipWhile(t => !t.Equals(features, StringComparison.InvariantCultureIgnoreCase))
                .Skip(1)
                .Take(1)
                .FirstOrDefault();
        }
    }
}
